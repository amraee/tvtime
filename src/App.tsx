import * as React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

import Upcoming from './pages/Upcoming';
import Calender from './pages/Calender';
import Explore from './pages/Explore';
import Profile from './pages/Profile';
import WatchList from './pages/WatchList';
import HomePage from './pages/HomePage'



const App:React.FC=()=> {
	return (
		<div>
<Router >

<Route path="/" component={WatchList}></Route>
<Route path="/Calender" component={Calender}></Route>
<Route path="/Profile" component={Profile}></Route>
<Route path="/Upcoming" component={Upcoming}></Route>
<Route path="/Explore" component={Explore}></Route>
</Router> 

		</div>
	);
}

export default App;

