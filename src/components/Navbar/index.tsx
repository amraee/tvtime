import * as React from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';


const Navbara = styled.a`
	background-color: #272727;
	overflow-y: scroll;
	height: 100%;
	width: 173px;
	padding: 10px 32px 10px 15px;
	position: fixed;
`;

const Logo = styled.a`
	text-align: center;
	width: 100%;
	display: block;
	display:block & > img {
		width: 38px;
	}
`;

const Search = styled.div`
	margin-top: 12px;
	position: relative;
	margin-bottom: 20px;
	width: 89%;
	& > input {
		font-size: 14px;
		font-family: ProximaNova;
		font-weight: 400;
		border: 1px solid #353535;
		color: #666;
		padding: 6px 6px 6px 26px;
		background-color: #222;
		width: 100%;
	}
	& > i {
		position: absolute;
		left: 10px;
		top: 9px;
		color: #666;
		font-size: 12px;
	}
`;

const StyledLink = styled(Link)`
	font-size: 14px;
	line-height: 20px;
	position: relative;
	padding: 9px 40px;
	display: block;

	& > span > i {
		position: absolute;
		left: 2px;
		font-size: 18px;
		top: 12px;
		color: #999;
	}

	& > span > span {
		font-size: 14px;
		color: #aaaaaa;
	}
`;
const Nav = styled.div`
	border-bottom: 1px solid #333;
	padding-bottom: 20px;
`;
const User = styled.div`
	& > span {
		font-size: 12px;
		color: #999999;
		line-height: 24px;
		display: inline-block;
		padding-top: 15px;
	}
	& > a {
		margin-left: 8px;
		color: #999999;
	}
	& > a > span {
		margin: 5px;
	}
`;
const Links = styled.div`
	border-bottom: 1px solid #333;
	padding-bottom: 40px;
	padding-top: 10px;
	& > a {
		font-size: 14px;
		line-height: 20px;
		position: relative;
		padding: 9px 40px;
		display: block;
		color: #aaaaaa;
	}
	& > a > span > i {
		position: absolute;
		left: 2px;
		font-size: 18px;
		top: 12px;
		color: #999;
	}
`;
const App = styled.div`
	& > h1 {
		font-size: 19px;
		line-height: 20.9px;
		color: #fff;
		font-weight: 700;
		margin: 30px 0;
	}
`;

const Tell = styled.div`
	& > input {
		font-size: 18px;
		line-height: 25.76px;
		width: 75%;
		padding: 10px 10px 10px 46px;
		border-radius: 3px;
		border: none;
	}
`;
const Like = styled.div`
	margin-bottom: 20px;
	& > a {
		border-radius: 3px;
		background-color: #fbd737;
		font-size: 18px;
		line-height: 25.76px;
		border: none;
		padding: 10px;
		margin-top: 5px;
		color: #333333;
		font-weight: 500;
		display: inline-block;
		text-align: center;
		width: 97.1%;
	}
`;
const Apps = styled.div`
	& > a {
		display: block;
		color: #888;
		font-weight: 700;
		font-size: 18px;
		text-align: center;
		border-radius: 6px;
		border: 1px solid #888;
		width: 92px;
		padding: 5px 0;
	}
`;

const Sign = styled.div`
	text-align: center;
	margin: 15px 0;

	& > a > i {
		position: absolute;
		left: -40px;
		top: 15px;
		font-size: 12px;
	}

	& > a > span {
		font-size: 12px;
		line-height: 17.15px;
		color: #aaaaaa;
		display: block;
		font-weight: 500;
		border-top: 1px solid #333;
		border-bottom: 1px solid #333;
		padding: 12px 0;
	}
`;

const Connect = styled.div`
	& > h1 {
		font-size: 12px;
		line-height: 14px;
		font-weight: 500;
		color: #999999;
		padding: 0 0 10px 0;
	}
`;
const Follow = styled.a`
	height: 20px;
	box-sizing: border-box;
	padding: 1px 8px 1px 6px;
	background-color: #1b95e0;
	color: #fff;
	border-radius: 3px;
	font-weight: 500;
	display: inline-block;
	font-size: 14px;
`;
const Likes = styled.a`
	display: inline-block;
	background: #4267b2;
	border: 1px solid #4267b2;
	color: #fff;
	font-family: Helvetica, Arial, sans-serif;
	border-radius: 5px;
	padding: 0 5px 2px 5px;
	margin-bottom: 10px;
`;

const Lang = styled.div`
	margin: 25px 0 0 0;
	border-top: 1px solid #333;
	padding: 20px 0 20px 0;
	border-bottom: 1px solid #333;
	text-align: center;
	color: #777777;
`;
const Select = styled.select`
	background-color: transparent;
	border: none;
	color: #aaa;
	font-size: 12px;
	line-height: 14.4px;
	display: block;
	margin-bottom: 15px;
`;
const Footer = styled.div`
	& > a {
		font-size: 11px;
		line-height: 17.15px;
		color: #777777;
	}
`;
const Navbar = () => {
	return (
		<Navbara>
			<Logo href="javascript:void(0)">
				<img src="https://www.tvtime.com/images/logo_flat.png" />
			</Logo>

			<Search>
				<input type="text" placeholder="Search" />
				<i className="fa fa-search" />
			</Search>

			<Nav>
				<StyledLink to="/Calender" href="javascript:void(0);">
					<span>
						<i className="fa fa-safari" />
						<span>Calender</span>
					</span>
				</StyledLink>
				<StyledLink to="/Upcoming" href="javascript:void(0);">
					<span>
						<i className="fa fa-safari" />
						<span>Upcoming</span>
					</span>
				</StyledLink>
				<StyledLink to="/" href="javascript:void(0);">
					<span>
						<i className="fa fa-list-ol" />
						<span>Watchlist</span>
					</span>
				</StyledLink>
				<StyledLink to="/Profile" href="javascript:void(0);">
					<span>
						<i className="fa fa-user-circle" />
						<span>Profile</span>
					</span>
				</StyledLink>
				<StyledLink to="/Explore" href="javascript:void(0);">
					<span>
						<i className="fa fa-safari" />
						<span>Explore</span>
					</span>
				</StyledLink>
			</Nav>

			<User>
				<span>ALIAMRAEE</span>
				<a href="javascrit:void(0)">
					<i className="fa fa-safari" />
					<span>0</span>
				</a>
				<a href="javascrit:void(0)">
					<i className="fa fa-safari" />
					<span>3</span>
				</a>

				<Links>
					<a href="javascript:void(0);">
						<span>
							<i className="fa fa-safari" />
							<span>Settings</span>
						</span>
					</a>
					<a href="javascript:void(0);">
						<span>
							<i className="fa fa-safari" />
							<span>Help</span>
						</span>
					</a>
				</Links>
			</User>

			<App>
				<h1>Open in App</h1>

				<Tell>
					<input type="text" placeholder="(201)555-0123" />
					<img src="flog.png" alt="" />
				</Tell>
				<Like>
					<a href="javascript:void(0)"> Text me a like</a>
				</Like>

				<Apps>
					<a href="javascript:void(0)">
						<i />
						<span>iphone</span>
					</a>
					<a href="javascript:void(0)">
						<i />
						<span>Android</span>
					</a>
				</Apps>
			</App>

			<Sign>
				<a href="javascript:void(0)">
					<i className="fa fa-sign-out" />
					<span>Sign out</span>
				</a>
			</Sign>

			<Connect>
				<h1>CONNECT WITH US</h1>
				<Likes href="javascript:void(0)">
					<i className="fa fa-thumbs-up" />
					<span>Like 392k</span>
				</Likes>
				<Follow href="javascript:void(0)">
					<i className="fa fa-twitter" />
					<span>Follow @tvshowtime</span>
				</Follow>
			</Connect>

			<Lang>
				<Select>
					<option value="">English</option>
					<option value="">English</option>
					<option value="">English</option>
					<option value="">English</option>
					<option value="">English</option>
					<option value="">English</option>
				</Select>

				<Footer>
					<a href="javascript:void(0)">Contect</a>-
					<a href="javascript:void(0)">About</a>-
					<a href="javascript:void(0)">Date & insights</a>-
					<a href="javascript:void(0)">Article</a>-
					<a href="javascript:void(0)">Podcasts</a>-
					<a href="javascript:void(0)">Developers</a>-
					<a href="javascript:void(0)">Privacy Policy</a>-
					<a href="javascript:void(0)">Terms thanks</a>
				</Footer>
			</Lang>
		</Navbara>
	);
};

export default Navbar;
