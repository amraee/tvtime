import * as React from 'react';
import styled from 'styled-components'

const Div = styled.div`
display:inline-block;
padding: 20px 9px;

&>img{
	width: 128px;
    height: 187px;
    border-radius: 2px;
}
&>a{
	font-size: 14px;
    line-height: 15.4px;
    color: #444444;
    font-weight: 500;
	margin: 5px 0 0px 0;
	display: block;
}
`


const BoxProfile = (props:any) => {
	return (
		<Div>
			<img src={props.src} />
			<a href="javascript:void(0)">Money Heist</a>
		</Div>
	);
};

export default BoxProfile;
