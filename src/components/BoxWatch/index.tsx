import * as React from 'react';
import styled from 'styled-components';

const Img = styled.img`
	width: 100%;
    height: 195px;
    border-radius: 2px;
`;
const Div = styled.div`
	width: 140px;
	display: inline-block;
	padding: 12px 17px;
`;
const Caption = styled.div`
	& > h1 {
		font-size: 14px;
		line-height: 15px;
		color: #444444;
		font-weight: 500;
		margin: 4px 0 2px;
		display: inline-block;
	}
	& > span {
		font-size: 12px;
		line-height: 13.2px;
		font-weight: 500;
		color: #999999;
		margin-left: 4px;
	}
	& > a {
		font-size: 11px;
		line-height: 17.2px;
		color: #666666;
		display: block;
		margin-top: 2px;
	}
`;

const Image = styled.div`
	position: relative;

	& > div {
		position: absolute;
		bottom: 1px;
		height: 4px;
		background-color: rgba(0, 0, 0, .5);
		z-index: 0;
		width: 100%;
	}
`;
const A = styled.a`
	text-decoration: none;
	color: #aaaaaa;
	text-align: end;
    padding: 2px 0;
    display:block;
    font-size:14px;
`;

interface props{
	src:string;
}

const BoxWatch = (props:props) => {

 	document.querySelectorAll('.btn').forEach((item) => {
		item.addEventListener('click', (event) => {
		console.log(event)
		});
	});
	return (
		<Div>
			<A className="btn" href="javascript:void(0)">
				<i className="fa fa-eye" />
			</A>
			<Image>
				<Img src={props.src} />
				<div />
			</Image>
			<Caption>
				<h1>S01E10</h1>
				<span>+ 4 episodes</span>
				<a href="javascript:void(0)">Why Women Kill</a>
			</Caption>
		</Div>
	);
};

export default BoxWatch;

