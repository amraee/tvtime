import * as React from 'react';
import styled from 'styled-components'

const Headera = styled.div`
  background: black;
  padding: 20px 30px;
  width:100%;
`

const Logo = styled.div`
display:inline-block;


&>img{
   width:40px 
   height:40px 
   display:inline-block
}

&>h1{
    color:white;
    margin-left 7px;
    font-size:24px;
    vertical-align:top;
    display:inline-block

}
`

const Nav =styled.div`
display:inline-block;

&>a{
    text-decoration:none;
    color: white ; 
}
`
const Join = styled.div`
display:inline-block;
float : right;
`
const Contactus = styled.a`
color:white;
text-decoration:none;
margin-right:10px;
padding:9px;
background:#222;
border-radius:60px;
font-size:14px
`
const Linkdin = styled.a`

&>img{
    width:20px;
}
`


const Header = () => {
	return (
		<Headera>
			<Logo>
				<img
					src="https://www.tvtime.com/ga-assets/1573082515008/static/3cfcb6be014c3d71f5bd5e2d0abc696f/95054/tvt_logo.png"
					alt=""
				/>
				<h1>TVLytice</h1>
			</Logo>

			<Nav>
				<a href="javascript:void(0)">Who We Are</a>
				<a href="javascript:void(0)">What We Do</a>
				<a href="javascript:void(0)">In The News</a>
				<a href="javascript:void(0)">WHat People Are Watching</a>
			</Nav>
        <Join>
        <Contactus href="javascript:void(0)">Contact Us</Contactus>

        <Linkdin href="javascript:void(0)">
            <img src="./linkdin.png" alt="" />
        </Linkdin>
        </Join>
		</Headera>
	);
};

export default Header;
