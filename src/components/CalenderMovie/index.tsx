import * as React from 'react';
import styled from 'styled-components';
const Div = styled.div`
	position: relative;
	width: 219px;
	padding: 0;
	margin: 12px;
    border: 1px solid #ddd;
    display:inline-block;
    text-align: center;
    background-color: #FBFBFB;
`;
const Img = styled.a`

&>img{
    width:100%;
}
`
const Text = styled.div`
text-align:center;
&>h1{
    font-size: 16px;
    line-height: 17.6px;
    font-weight: 600;
    color: #444444;
    margin: 10px auto 5px;
}
&>span{
    font-size: 12px;
    line-height: 17.15px;
    color: #777777;
}
`
const Time= styled.div`
text-align:center;
&>span{
    width: 193px;
    margin: 5px auto 0;
    border-top: 1px solid #ececec;
    font-size: 13px;
    padding: 8px 0;
    margin-top: 32px;
    display:block;
}
`
const Icon=styled.div`
position: absolute;
top: -17px;
right: 1px;
color: #3f3f3f;
font-size: 14px;
&>i{

}
`
interface props{
	src:string;
}

const Movie = (props:props) => {
	return (
		<Div>
			<Icon>
				<i className="fa fa-eye" />
			</Icon>
			<Text>
				<Img href="javascript:void(0)">
					<img src={props.src} />
				</Img>
				<h1>S06E10</h1>
				<span>the 100</span>

				<Time>
					<span>21:00 on The CW</span>
				</Time>
			</Text>
		</Div>
	);
};

export default Movie;
