import * as React from 'react';
import styled from 'styled-components';

const Fixed = styled.div`
	border-left: 1px solid #ccc;
	position: fixed;
	top: 0;
	bottom: 0;
	width: 190px;
	background-color: #f2f2f2;
	right: 0;
	padding: 12px;
`;
const Filter = styled.div`
	& > h1 {
		font-size: 16px;
		line-height: 24px;
		color: #666666;
		font-weight: 500;
	}
	& > p {
		font-size: 11px;
		line-height: 15.7px;
		color: #777777;
		font-weight: 500;
		margin-top: 15px;
	}
	& > select {
		font-size: 16px;
		line-height: 19.2px;
		width: 100%;
		margin: 5px 0 15px;
		background: #eee;
		color: #666;
		border-radius: 2px;
		border: 1px solid #bbb;
	}
`;
const Dates = styled.div`
	padding: 10px 0 0 0;
	border-top: 1px solid #ddd;
`;

const Thead = styled.thead`
	& > tr > th {
		color: #666666;
		font-size: 14px;
		line-height: 27px;
		font-weight: 600;
		padding: 0 0px;
	}
`;

const Tbody = styled.tbody`
	& > hr > th {
		padding: 0 5px;
		font-size: 14px;
		line-height: 28px;
		font-weight: 500;
		color: #666;
		border-radius: 4px;
	}
	& > tr > th > a {
		padding: 0 4px;
		font-size: 14px;
		line-height: 28px;
		font-weight: 500;
		color: #666;
		border-radius: 4px;
	}
`;

const Tfoot = styled.tfoot`
	& > hr > th {
	}
	& > tr > th > a {
		font-size: 14px;
		line-height: 27px;
		font-weight: 600;
		color: #666666;
	}
`;

const BoxCalender = () => {
	return (
		<Fixed>
			<Filter>
				<h1>TV Calender </h1>
				<p>CHOOSE WHAT TO DISPLAY</p>
				<select>
					<option>My shows</option>
					<option>All shows</option>
					<option>My shows on hiatus</option>
					<option>All shows on hiatus</option>
					<option>New shows</option>
				</select>
			</Filter>

			<Dates>
				<table>
					<Thead>
						<tr>
							<th>«</th>
							<th>July 2019</th>
							<th>July 2019</th>
							<th>»</th>
						</tr>
						<tr>
							<th>Su</th>
							<th>Mu</th>
							<th>Tu</th>
							<th>We</th>
							<th>Th</th>
							<th>Fr</th>
							<th>Sa</th>
						</tr>
					</Thead>
					<Tbody>
						<tr>
							<th>
								<a href="javascrit:void(0)">30</a>
							</th>
							<th>
								<a href="javascript:void(0)">1</a>
							</th>
							<th>
								<a href="javascript:void(0)">2</a>
							</th>
							<th>
								<a href="javascript:void(0)">3</a>
							</th>
							<th>
								<a href="javascript:void(0)">4</a>
							</th>
							<th>
								<a href="javascript:void(0)">5</a>
							</th>
							<th>
								<a href="javascript:void(0)">6</a>
							</th>
						</tr>
						<tr>
							<th>
								<a href="javascript:void(0)">7</a>
							</th>
							<th>
								<a href="javascript:void(0)">8</a>
							</th>
							<th>
								<a href="javascript:void(0)">9</a>
							</th>
							<th>
								<a href="javascrip:void(0)">10</a>
							</th>
							<th>
								<a href="javascrip:void(0)">11</a>
							</th>
							<th>
								<a href="javascrip:void(0)">12</a>
							</th>
							<th>
								<a href="javascrip:void(0)">13</a>
							</th>
						</tr>
						<tr>
							<th>
								<a href="javascrip:void(0)">14</a>
							</th>
							<th>
								<a href="javascrip:void(0)">15</a>
							</th>
							<th>
								<a href="javascrip:void(0)">16</a>
							</th>
							<th>
								<a href="javascrip:void(0)">17</a>
							</th>
							<th>
								<a href="javascrip:void(0)">18</a>
							</th>
							<th>
								<a href="javascrip:void(0)">19</a>
							</th>
							<th>
								<a href="javascrip:void(0)">20</a>
							</th>
						</tr>
						<tr>
							<th>
								<a href="javascrip:void(0)">21</a>
							</th>
							<th>
								<a href="javascrip:void(0)">22</a>
							</th>
							<th>
								<a href="javascrip:void(0)">23</a>
							</th>
							<th>
								<a href="javascrip:void(0)">24</a>
							</th>
							<th>
								<a href="javascrip:void(0)">25</a>
							</th>
							<th>
								<a href="javascrip:void(0)">26</a>
							</th>
							<th>
								<a href="javascrip:void(0)">27</a>
							</th>
						</tr>
						<tr>
							<th>
								<a href="javascrip:void(0)">28</a>
							</th>
							<th>
								<a href="javascrip:void(0)">29</a>
							</th>
							<th>
								<a href="javascrip:void(0)">30</a>
							</th>
							<th>
								<a href="javascrip:void(0)">31</a>
							</th>
							<th>
								<a href="javascript:void(0)">1</a>
							</th>
							<th>
								<a href="javascript:void(0)">2</a>{' '}
							</th>
							<th>
								<a href="javascript:void(0)">3</a>{' '}
							</th>
						</tr>
						<tr>
							<th>
								<a href="javascript:void(0)">4</a>
							</th>
							<th>
								<a href="javascript:void(0)">5</a>
							</th>
							<th>
								<a href="javascript:void(0)">6</a>
							</th>
							<th>
								<a href="javascript:void(0)">7</a>
							</th>
							<th>
								<a href="javascript:void(0)">8</a>
							</th>
							<th>
								<a href="javascript:void(0)">9</a>
							</th>
							<th>
								<a href="javascript:void(0)">10</a>
							</th>
						</tr>
					</Tbody>
					<Tfoot>
						<tr>
							<th>
								<a href="javascript:void(0)">Today</a>
							</th>
						</tr>
					</Tfoot>
				</table>
			</Dates>
		</Fixed>
	);
};

export default BoxCalender;
