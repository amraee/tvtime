import * as React from 'react';
import styled from 'styled-components';
import { render } from 'react-dom';
import { isTemplateElement } from '@babel/types';

const Img = styled.img`
	width: 100%;
	height: 195px;
	border-radius: 2px;
`;
const Div = styled.div`
	width: 140px;
	display: inline-block;
	padding: 12px 17px;
`;
const Caption = styled.div`
	& > a {
		font-size: 12px;
		line-height: 14.2px;
		color: #666666;
		display: block;
		margin-top: 2px;
	}
`;

const Image = styled.div`
	position: relative;

	& > div {
		position: absolute;
		top: 0px;
		height: 100%;
		width: 100%;
	}
`;
const A = styled.a`
	text-decoration: none;
	color: #aaaaaa;
	text-align: end;
	padding: 2px 0;
	display: block;
	font-size: 14px;
	& > span {
		font-size: 11px;
		color: #fff;
		background: #aaa;
		padding: 3px 8px 0;
		display: inline-block;
		border-radius: 2px;
		margin-bottom: 2px;
		margin-right: 1px;
	}
`;
const Progress = styled.div`
	background-color: #444;
	height: 30px;
	line-height: 30px;
	border-bottom: 1px solid #ddd;
	border-radius: 3px 3px 0 0;
	color: #fff;
	font-size: 13px;
	text-align: center;
	margin-bottom: 35px;
`;
const Save = styled.div`
	position: absolute;
	left: 0;
	right: 0;
	bottom: 0;
	color: #fff;
	background: #f5d248;
	height: 28px;
	text-align: center;
	font-weight: 700;
	font-size: 13px;
	line-height: 28px;
`;


const None = styled.div`
	background-color: #f0f0f0;
	display: none;
`;
const Form = styled.div`
	& > select {
		width: 104px;
		padding: 4px 10px;
		background: #fff;
		border: 1px solid #ccc;
		color: #555;
		font-weight: unset;
		margin: 19px 17px;
		margin-top: 0;
	}
`;
interface props{
	src:string;
}


const BoxEplore = (props:props) => {

	return (
		<Div>
			<A className="btn" href="javascript:void(0)">
				<span>Add</span>
			</A>
			<Image>
				<Img src={props.src} />
				<None>
					<Progress>Progression</Progress>
					<select>
						<Form>
							<select>
								<option>Seasone 1 </option>
								<option>Seasone 1 </option>
								<option>Seasone 1 </option>
							</select>
							<select>
								<option>None episod</option>
								<option>None episod</option>
								<option>None episod</option>
							</select>
						</Form>
					</select>
					<Save>save</Save>
				</None>
			</Image>
			<Caption>
				<a href="javascript:void(0)">Chernobyl</a>
				<a href="javascript:void(0)">525,481 followers</a>
			</Caption>
		</Div>
	);
};

export default BoxEplore;
