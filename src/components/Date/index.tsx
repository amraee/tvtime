import * as React from 'react';
import styled from 'styled-components';
import { number } from 'prop-types';

const Div = styled.div`
	font-family: DinotCB;
	text-align: center;
	background-color: transparent;
	padding-top: 20px;
	width: 185px;
	display: inline-block;
    
	& > span {
		font-size: 23px;
		line-height: 32.85px;
		color: #c6c6c6;
	}
	& > h1 {
		font-size: 100px;
		line-height: 80px;
		color: #c6c6c6;
		display: block;
	}
	& > h3 {
		font-size: 27px;
		line-height: 24px;
		color: #444444;
		font-weight: 400;
	}
	& > div {
		border-top: 1px solid #ececec;
		padding-top: 2px;
		width: 150px;
		margin: 2px auto 0;
	}
	& > div > p {
		line-height: 25.7165px;
		font-size: 18px;
		color: #777777;
	}
`;

interface props{
	days:number;
}

const Date = (props:props) => {
	return (
		<Div>
			<h3>Tuesay</h3>
			<h1>{props.days}</h1>
			<span>July</span>

			<div>
				<p>in 3 day</p>
			</div>
		</Div>
	);
};
export default Date;
