import * as React from 'react';
import Navbar from '../components/Navbar';
import styled from 'styled-components';
import BoxCalender from '../components/BoxCalender';
import Date from '../components/Date';
import CalenderMovie from '../components/CalenderMovie';

const Body = styled.div`
	margin-left: 221px;
	padding: 20px 12px 0;

	& > div {
        padding-bottom: 50px;
        width: 1009px;
	}
`;

const H1 = styled.h1`
	font-size: 19px;
	color: #666666;
	font-weight: 500;
	margin-left: 17px;
	margin-bottom: 10px;
`;

const Calender = ()=> {
	return (
		<div>
			<Navbar />
			<Body>
				<div>
					<BoxCalender />
					<Date days={1} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/330134/1236640-2-q80.jpg" />


					<Date days={2} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/71424/1017201-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/275224/1000645-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />


					<Date days={3} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/330134/1236640-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/71424/1017201-2-q80.jpg" />
					<Date days={4} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/351187/1299582-2-q80.jpg" />

					<Date days={5} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/83196/1158898-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/365069/1380882-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<Date days={6} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/275224/1000645-2-q80.jpg" />
					<Date days={7} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/268298/951268-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/250487/1381711-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/351187/1299582-2-q80.jpg" />
					<Date days={8} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/83196/1158898-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/365069/1380882-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/250487/1381711-2-q80.jpg" />

					<Date days={9} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/83196/1158898-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/73762/1390589-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/78901/1397362-2-q80.jpg" />
					<Date days={10} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/330134/1236640-2-q80.jpg" />
					<Date days={11} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/351187/1299582-2-q80.jpg" />
					<Date days={12} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/83196/1158898-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/365069/1380882-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/250487/1381711-2-q80.jpg" />
					<Date days={13} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/71424/1017201-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/275224/1000645-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<Date days={14} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/351187/1299582-2-q80.jpg" />
					<Date days={15} />					
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />				
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/275224/1000645-2-q80.jpg" />

						<Date days={16} />					
						<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/79838/1132974-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/75332/1031232-2-q80.jpg" />
					<CalenderMovie src="https://dg31sz3gwrwan.cloudfront.net/fanart/330134/1236640-2-q80.jpg" />
					
				</div>
			</Body>
		</div>
	);
}

export default Calender;
