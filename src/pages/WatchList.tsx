import * as React from 'react';
import Navbar from '../components/Navbar';
import BoxWatch from '../components/BoxWatch';
import styled from 'styled-components';
import { Interface } from 'readline';

const Body = styled.div`
	margin-left: 221px;
	padding: 20px 12px 0;

	& > div {
		padding-bottom: 50px;
		display: inline-block;
	}
`;

const H1 = styled.h1`
	font-size: 19px;
	color: #666666;
	font-weight: 500;
	margin-left: 17px;
	margin-bottom: 10px;
`;

const Img = styled.img`
	width: 100%;
	height: 195px;
	border-radius: 2px;
`;
const Div = styled.div`
	width: 140px;
	display: inline-block;
	padding: 12px 17px;
`;
const Caption = styled.div`
	& > h1 {
		font-size: 14px;
		line-height: 15px;
		color: #444444;
		font-weight: 500;
		margin: 4px 0 2px;
		display: inline-block;
	}
	& > span {
		font-size: 12px;
		line-height: 13.2px;
		font-weight: 500;
		color: #999999;
		margin-left: 4px;
	}
	& > a {
		font-size: 11px;
		line-height: 17.2px;
		color: #666666;
		display: block;
		margin-top: 2px;
	}
`;

const Image = styled.div`
	position: relative;

	& > div {
		position: absolute;
		bottom: 1px;
		height: 4px;
		background-color: rgba(0, 0, 0, .5);
		z-index: 0;
		width: 100%;
	}
`;
const A = styled.a`
	text-decoration: none;
	color: #aaaaaa;
	text-align: end;
	padding: 2px 0;
	display: block;
	font-size: 14px;
`;

const Progress = styled.div`
	width:1px;
	background:#009b00;
	height:100%;
`


const Watch = (): any => {
	const [ icon, setIcon ] = React.useState('fa fa-eye');
	const [ progress, setProgress ] = React.useState(0);
	return (
		<div>
			<Navbar />
			<Body>
				<div>
					<H1>Watch next (3)</H1>
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
			
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>

					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
			
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
			
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
		
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
			
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
			
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
	
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
		
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>	
				
				
				
				<H1>Watch next (3)</H1>
				<div>
				
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>



				<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>

			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>

			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>

			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
				<div>
				
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>


			<H1>Watch next (3)</H1>

			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>
			<div>
				
				<Div>
					<A
						onClick={() => {
							setIcon('fa fa-check');
							setProgress(progress+10);
							if(progress==140){
								setProgress(140)
							}
						}}
						className="btn"
						href="javascript:void(0)"
					>
						<i className={icon} />
					</A>
					<Image>
						<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
						<div>
							<Progress style={{width:progress}} />
						</div>
					</Image>
					<Caption>
						<h1>S01E10</h1>
						<span>+ 4 episodes</span>
						<a href="javascript:void(0)">Why Women Kill</a>
					</Caption>
				</Div>
			</div>

				<div>
				
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
					<div>
				
					<Div>
						<A
							onClick={() => {
								setIcon('fa fa-check');
								setProgress(progress+10);
								if(progress==140){
									setProgress(140)
								}
							}}
							className="btn"
							href="javascript:void(0)"
						>
							<i className={icon} />
						</A>
						<Image>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
							<div>
								<Progress style={{width:progress}} />
							</div>
						</Image>
						<Caption>
							<h1>S01E10</h1>
							<span>+ 4 episodes</span>
							<a href="javascript:void(0)">Why Women Kill</a>
						</Caption>
					</Div>
				</div>
				
			</Body>
		</div>
	);
};

export default Watch;

/*

					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/71424/1017200-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/365069/1381079-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/365069/1381079-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/75332/1220753-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/83196/945777-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/264586/1294097-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/305288/1372844-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/268592/1276791-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/331600/1394500-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/153021/1298939-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/257655/1396659-4-optimized.jpg" />
				</div>
				<div>
					<H1>Watch next (3)</H1>
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/73762/1094999-4-optimized.jpg" />
				</div>
				<div>
					<H1>Watch next (3)</H1>
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/365722/1374427-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/273242/1035394-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/268298/1006883-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/275224/1000644-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/351187/1299573-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/346003/1296784-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/319861/1294658-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/320593/1369725-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/335425/1374435-4-optimized.jpg" />
					<BoxWatch src="https://dg31sz3gwrwan.cloudfront.net/poster/361585/1350056-4-optimized.jpg" />
				</div>
*/
