import * as React from 'react';
import Navbar from '../components/Navbar';
import BoxProfile from '../components/BoxProfile';
import styled from 'styled-components';

const Body = styled.div`
	margin-left: 220px;
	position: relative;
`;

const H1 = styled.h1`
	font-size: 19px;
	color: #666666;
	font-weight: 500;
	margin-left: 17px;
	margin-bottom: 10px;
`;

const Head = styled.div`
	display: flex;

	& > img {
		width: 20%;
	}
`;

const Nav = styled.nav`
	position: relative;
	border-bottom: 1px solid #ddd;
	display: block;
	padding-left: 150px;

	& > a {
		font-size: 14px;
		font-weight: 700;
		color: #282828;
		padding: 12px 20px;
		display: inline-block;
	}
`;
const User = styled.div`
	display: block;
	position: absolute;
	top: -40px;
	left: 15px;
	& > img {
		width: 100px;
		height: 100px;
		border-radius: 50%;
	}

	& > h1 {
		position: absolute;
		top: 12px;
		font-size: 20px;
		line-height: 22px;
		font-weight: 500;
		color: #fff;
		left: 113px;
	}
`;

const Shows = styled.div`
	padding: 15px 48px;

	& > h1 {
		display: inline-block;
		margin: 20px 0 0 9px;
		font-size: 16px;
		line-height: 17.6px;
		color: #222222;
		font-weight: 550;
	}
	& > select {
		background-color: #e5e5e5;
		padding: 10px 34px;
		border-radius: 4px 4px 0 0;
		margin: 10px 0 6px 15px;
		position: relative;
		top: -5px;
		border: none;
		display: inline-block;
	}
`;
const Image = styled.div``;

const Div = styled.div`
	position: relative;
	top: -135px;
	background: white;
`;
const Profile = () => {
	return (
		<div>
			<Navbar />
			<Body>
				<Head>
					<img src="https://dg31sz3gwrwan.cloudfront.net/poster/323168/1364198-0-optimized.jpg" />
					<img src="https://dg31sz3gwrwan.cloudfront.net/poster/257655/1235485-0-optimized.jpg" />
					<img src="https://dg31sz3gwrwan.cloudfront.net/poster/253463/1366784-0-optimized.jpg" />
					<img src="https://dg31sz3gwrwan.cloudfront.net/poster/327417/1261189-0-optimized.jpg" />
					<img src="https://dg31sz3gwrwan.cloudfront.net/poster/264586/1294097-0-optimized.jpg" />
				</Head>
				<Div>
					<Nav>
						<User>
							<img src="https://d36rlb2fgh8cjd.cloudfront.net/default-images/default-user-q80.png" />
							<h1>aliamraee</h1>
						</User>
						<a href="javascrip:void(0)"> SHOWS </a>
						<a href="javascrip:void(0)"> FEED </a>
						<a href="javascrip:void(0)"> FOLLOWING </a>
						<a href="javascrip:void(0)"> STATS </a>
					</Nav>

					<Shows>
						<h1>ALL SHOWS</h1>
						<select>
							<option>ALL</option>
							<option>Late</option>
							<option>Up to date</option>
							<option>Ended</option>
						</select>

						<Image>
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/323168/1364198-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/250487/1298931-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/257655/1235485-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/253463/1366784-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/81189/913598-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/121361/1061959-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/73762/1308148-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/327417/1261189-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/264586/1294097-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/146711/873797-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/311954/1313148-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/176941/1157562-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/305288/1372844-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/78901/1310510-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/268592/1276791-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/80379/1304753-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/279121/1333055-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/95491/1366299-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/95491/1366299-4-optimized.jpg" />
							<BoxProfile src="https://dg31sz3gwrwan.cloudfront.net/poster/260449/1319817-4-optimized.jpg" />
						</Image>
					</Shows>
				</Div>
			</Body>
		</div>
	);
};

export default Profile;
