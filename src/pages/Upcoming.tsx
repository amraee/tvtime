import * as React from 'react';
import Navbar from '../components/Navbar';
import styled from 'styled-components';

const Body = styled.div`
	margin-left: 221px;
	padding: 20px 12px 0;

	& > div {
		padding-bottom: 50px;
	}
`;

const H1 = styled.h1`
	font-size: 19px;
	color: #666666;
	font-weight: 500;
	margin-left: 17px;
	margin-bottom: 10px;
`;

const Div = styled.div`
	width: 140px;
	display: inline-block;
	padding: 12px 17px;
	& > div {
		position: relative;
		text-align: center;
	}
`;

const Caption = styled.div`
	& > span {
		font-size: 12px;
		line-height: 13.2px;
		font-weight: 500;
		color: #999999;
		margin-left: 4px;
	}
	& > a {
		font-size: 11px;
		line-height: 17.2px;
		color: #666666;
		display: block;
		margin-top: 2px;
	}
`;
const Img = styled.img`
	width: 100%;
	height: 195px;
	border-radius: 2px;
	&::before {
		content: '';
    width: 84%;
    height: 77%;
    position: absolute;
    background: black;
    opacity: 0.6;
    border-radius: 3px;
	}
`;

const Days = styled.div`
	position: absolute;
	top: 43%;
	display: block;
	width: 100%;
	/* background: rgba(0,0,0,0.5); */
	height: 100%;
`;
const Progress=styled.div`

& > div {
    position: absolute;
    bottom: 1px;
    height: 4px;
    background-color: rgba(0, 0, 0, .5);
    width: 100%;
}
`
const Upcoming = () => {
	return (
		<div>
			<Navbar />
			<Body>
				<div>
					<H1>Watch next (3)</H1>
					<Div>
						<div>
							<Img src="https://dg31sz3gwrwan.cloudfront.net/poster/339369/1323280-4-optimized.jpg" />
							<Days>
								<h6>97</h6>
								<p>Days</p>
							</Days>
							<Progress>
								<div id="progress" />
							</Progress>
						</div>
						<Caption>
							<a href="javascript:void(0)">S08E01</a>
							<span>Arrow</span>
						</Caption>
					</Div>
				</div>
			</Body>
		</div>
	);
};

export default Upcoming;
